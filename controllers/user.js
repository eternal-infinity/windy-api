const Users = require("../models").user;
require("dotenv").config();
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const Token = require('../models').token;
const reset = require('../models').reset;
const sendEmail = require('../utils/sendEmail');
const crypto = require('crypto')


const registeruser = async (req, res) => {
    try {
        const data = req.body;

        if (!(data.email && data.firstname && data.lastname && data.pass)) {
            return res.status(400).send({
                message: "Fill all data", flag: "danger"
            });
        }

        const user = await Users.findOne({
            where: {
                email: data.email,
            },
        });

        if (user) {

            return res.status(409).send({
                message: "User already registered.", flag: "danger"
            });
        }

        else {

            const ePass = await bcrypt.hash(data.pass, 10);
            console.log(data, ePass)
            const usr = await Users.create({
                firstname: data.firstname,
                lastname: data.lastname,
                email: data.email,
                pass: ePass,
            });
            const newToken = await Token.create({
                userId: data.email,
                token: crypto.randomBytes(32).toString("hex"),
            });
            const url = "http://localhost:3000/" + data.email + "/" + newToken.token;
            const mail = await sendEmail(usr.email, "Verification Mail", url);
            return res.status(200).send({
                message: "Please verify the email sent",
                user: usr,
                flag: "success"
            });
        }

    } catch (error) {
        console.error(error);
        return res.status(500).send({ message: "Server Error. Try again.", flag: "danger" });
    }
}

const login = async (req, res) => {
    try {
        const data = req.body;
        console.log(data)
        if (!(data.email && data.pass)) {
            return res.status(400).send({
                message: "Fill all data", flag: "danger"
            });
        }

        const user = await Users.findOne({
            where: {
                email: data.email,
            },
        });
        if (!user) {
            return res.status(400).send({
                message: "User not registered", flag: "danger"
            });
        }
        if (!user.verified) {
            let etoken = await Token.findOne({ userId: user.email });
            if (!etoken) {
                etoken = await Token.create({
                    userId: user.email,
                    token: crypto.randomBytes(32).toString("hex"),
                });
                const url = "http://localhost:3000/" + data.email + "/" + etoken.token;
                const mail = await sendEmail(user.email, "Verification Mail", url);
            }

            return res.status(400).send({
                message: "An Email sent to your account please verify"
            });
        }
        else if (user && (await bcrypt.compare(data.pass, user.pass))) {
            const code = crypto.randomBytes(3).toString("hex");
            const rst = await reset.create({
                email: user.email,
                token: code,
            })
            const content = "This is the verification code: " + code;
            const mail = await sendEmail(user.email, "2 factor Auth", content);
            res.status(200).send({
                message: "Authentcation code sent successfully",
                flag: "success"
            });
        }
        else {
            return res.status(400).send({
                message: "Invalid Credentials", flag: "danger"
            });
        }


    } catch (error) {
        console.error(error);
        return res.status(500).send({ message: "Server Error. Try again.", flag: "danger" });
    }
}

const loginVerify = async (req, res) => {
    try {
        const user = await reset.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (!user) return res.status(400).send({ message: "User not registered", flag: "danger" });
        if (user.token == req.body.code) {
            user.destroy();
            const token = jwt.sign(
                { id: req.body.email },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h",
                }
            );
            console.log(token)


            return res.status(200).send({
                message: "Login Successful",
                token: token,
                email: user.email,
                flag: "success"
            });

        }
        else {
            res.status(400).send({ message: "Invalid code", flag: "danger" });
        }

    } catch (error) {
        console.error(error);
        return res.status(500).send({ message: "Server Error. Try again.", flag: "danger" });
    }

}


const verifyMail = async (req, res) => {
    try {
        console.log(req.params)
        const user = await Users.findOne({
            where: {
                email: req.params.email,
            },
        });
        if (!user) return res.status(400).send({ message: "Invalid link", flag: "danger" });

        const token = await Token.findOne({
            userId: user.email,
            token: req.params.token,
        });
        if (!token) return res.status(400).send({ message: "Invalid link", flag: "danger" });

        user.verified = true;
        await user.save();
        await token.destroy();

        res.status(200).send({
            message: "Email verified successfully",
            flag: "success"
        });
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}

const resetPass = async (req, res) => {
    try {
        const user = await Users.findOne({
            where: {
                email: req.params.email,
            },
        });
        if (!user) return res.status(400).send({ message: "User not registered", flag: "danger" });
        const code = crypto.randomBytes(3).toString("hex");
        const rst = await reset.create({
            email: user.email,
            token: code,
        })
        const content = "This is the verification code: " + code;
        const mail = await sendEmail(user.email, "Reset Password", content);
        res.status(200).send({
            message: "Verification code successfully",
            flag: "success"
        });
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}
const verifyCode = async (req, res) => {
    try {
        const user = await reset.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (!user) return res.status(400).send({ message: "User not registered", flag: "danger" });
        if (user.token == req.body.code) {
            user.destroy();
            res.status(200).send({
                message: "Verification successful",
                flag: "success"
            });
        }
        else {
            res.status(400).send({ message: "Invalid code", flag: "danger" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}

const changePass = async (req, res) => {
    try {
        console.log(req)
        const user = await Users.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (!user) return res.status(400).send({ message: "User not registered" });
        if (req.body.pass == req.body.conpass) {
            const ePass = await bcrypt.hash(req.body.pass, 10);
            user.pass = ePass;
            user.save();
            res.status(200).send({
                message: "Password Changed Successfully",
                flag: "success"
            });
        }
        else {
            res.status(400).send({ message: "Passwords didn't match", flag: "danger" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}

const getWeatherCoordinates = async (req, res) => {
    try {
        const data = req.body;
        await fetch(`${process.env.API_URL}/weather?lat=${data.lat}&lon=${data.long}&units=metric&APPID=${process.env.API_KEY}`)
            .then(res => res.json())
            .then(result => {
                console.log(result);
                res.status(200).send({ message: "Weather Data", data: result });
            });

    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}

const getWeatherCity = async (req, res) => {
    https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid={API key}
    try {
        const data = req.body;
        await fetch(`${process.env.API_URL}/weather?q=${data.city}&units=metric&APPID=${process.env.API_KEY}`)
            .then(res => res.json())
            .then(result => {
                console.log(result);
                res.status(200).send({ message: "Weather Data", data: result });
            });

    } catch (error) {
        console.log(error);
        res.status(500).send({ message: "Internal Server Error", flag: "danger" });
    }
}

module.exports = { registeruser, login, loginVerify, verifyMail, resetPass, verifyCode, changePass, getWeatherCity, getWeatherCoordinates }