require('dotenv').config();

const mysql = require('mysql2/promise');

const db = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'Wolf@707',
    database: 'windy',
    port: '3306',
    dialect: "mysql"
});

// const db = mysql.createPool({
//     host: process.env.DB_HOST,
//     user: process.env.DB_USER,
//     password: process.env.DB_PASS,
//     database: process.env.DB_NAME,
//     port: process.env.DB_PORT,
//     "dialect": "mysql"
// });

module.exports = db;


