'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class reset extends Model {
    
    static associate(models) {
     
    }
  };
  reset.init({
    email: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    token: {
        type: DataTypes.STRING,
        allowNull: false,
      },
  }, {
    sequelize,
    modelName: 'reset',
  });
  return reset;
};