'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class token extends Model {
    
    static associate(models) {
     
    }
  };
  token.init({
    userId: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    
  }, {
    sequelize,
    modelName: 'token',
  });
  return token;
};