const router = require("express").Router();
const authMiddleware = require("../middleware/auth");
const user = require("../controllers/user");

router.post(
	"/",
	user.login,
);

router.post (
    "/verify",
    user.loginVerify,
)


module.exports = router;