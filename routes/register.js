const router = require("express").Router();
const authMiddleware = require("../middleware/auth");
const user = require("../controllers/user");

router.post("/", user.registeruser);
router.get("/:email/:token", user.verifyMail)
module.exports = router;