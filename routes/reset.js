const router = require("express").Router();
const authMiddleware = require("../middleware/auth");
const user = require("../controllers/user");

router.get(
    "/:email",
    user.resetPass,
);

router.post("/", user.verifyCode,)


module.exports = router;