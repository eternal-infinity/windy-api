var express = require('express');
var router = express.Router();
const authMiddleware = require("../middleware/auth");
const user = require("../controllers/user");

router.post("/coordinates",
    authMiddleware.tokenAuth,
    user.getWeatherCoordinates,
)
router.post("/city",
    authMiddleware.tokenAuth,
    user.getWeatherCity,
)


module.exports = router;
